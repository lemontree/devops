# DevOps Demo

Demo setup showing DevOps concepts:

* CI/CD

## Components

| Component | Role | Endpoint | Login |
|-----------|------|----------|-------|
| Jenkins (Blue Ocean) | CI/CD | http://localhost:8080 | `admin`/`admin` |
| RabbitMQ | Messaging | N/A | N/A |